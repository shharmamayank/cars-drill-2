
function problem6(inventory) {
    let arr = []
    if (inventory === undefined || inventory.constructor != Array || inventory.length === 0) {
        return arr
    } else {
        for (let i = 0; i < inventory.length; i++) {

            if ((inventory[i].car_make == "Audi") || (inventory[i].car_make == "BMW")) {
                arr.push(inventory[i])
            }
        }
        return JSON.stringify(arr)
    }
}
module.exports = problem6