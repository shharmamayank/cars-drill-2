function Problem3(inventory) {
    let arr = []
    if (inventory === undefined || inventory.constructor != Array || inventory.length === 0) {
        return arr
    }
    else {
        for (let i = 0; i < inventory.length; i++) {
            let mod = inventory[i].car_model
            let upper = mod.toUpperCase()
            arr.push(upper)
        }
        arr.sort()
        return arr
    }
}

module.exports = Problem3